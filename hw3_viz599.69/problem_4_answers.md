1. d3.selectAll("tbody tr")[0].length-1 selects all element nodes that returns all rows within the table as multidimensional arrays. With length-1, it will count the amount of rows within the table and return them as index 0.

2. color(0) returns the value of red, which is ff4500. color(10) returns the value f25e26 that will slowly degrade the color of red to silver/gray. color(150) returns the value of silver/gray, which is 42ffff.

3. The min and max values of the rate can be represented through the colors as red and silver/gray respectively. Using the colors, we can say rows with minimum value are more red than rows with maximum value which are more silver to represent the unemployment rate.