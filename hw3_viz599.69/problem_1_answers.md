1. colspan="3" adjusts the table header elements to only 3 columns. 

2. 1) padding:3px is a CSS style that defines the size of spacing between the table header elements.
   2) ALIGN=Center is an HTML attribute that aligns the text to the center of the table header elements. The CSS equivalent is text-align: center.
   3) margin:0 is a CSS style that sets the sides top, bottom, left, and right margins to 0.

3. The differene between DOM inspector and the HTML source is that in DOM inspector it shows the element nodes as they are processing. HTML source shows the element nodes after being parsed. DOM inspector also lets you view the webpage while the nodes are being processed while HTML source nodes are not directly viewable. I would use DOM inspector when looking at specific element nodes and testing minor changes to the CSS so it can be viewed as it parses. I would use HTML source when I have to test a large code, which is much faster with HTML source than DOM inspector. 